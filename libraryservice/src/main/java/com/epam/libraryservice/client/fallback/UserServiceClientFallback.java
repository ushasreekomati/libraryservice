package com.epam.libraryservice.client.fallback;

import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.ResponseEntity;
import com.epam.libraryservice.client.UserServiceClient;
import com.epam.libraryservice.model.User;

import feign.FeignException;

public class UserServiceClientFallback implements UserServiceClient {

	private final Throwable cause;
	private static final Logger log = LogManager.getLogger(UserServiceClientFallback.class);

	public UserServiceClientFallback(Throwable cause) {
		this.cause = cause;
	}

	@Override
	public ResponseEntity<List<User>> getUsers() {
		getCauseOfFallback();
		List<User> userList = new ArrayList<>();
		userList.add(getDummyUser());
		return ResponseEntity.ok((userList));
	}

	@Override
	public ResponseEntity<User> addNew(User user) {
		getCauseOfFallback();
		return ResponseEntity.ok(getDummyUser());
	}

	@Override
	public ResponseEntity<User> update(Long userId, @Valid User user) {
		getCauseOfFallback();
		return ResponseEntity.ok(getDummyUser());
	}

	@Override
	public ResponseEntity<User> get(Long userId) {
		getCauseOfFallback();
		return ResponseEntity.ok(getDummyUser());
	}

	@Override
	public ResponseEntity<?> delete(Long userId) {
		getCauseOfFallback();
		return ResponseEntity.ok("Service is Currently Unavailable.Try After Sometime");
	}

	private void getCauseOfFallback() {
		if (cause instanceof FeignException && ((FeignException) cause).status() == 404) {
			log.error("Service Unavilable while fetching all books , Error Message :" + cause.getLocalizedMessage());
		} else {
			log.error("Other Error took place:" + cause.getLocalizedMessage());
		}
	}

	private User getDummyUser() {
		return new User(11111L, "Dummy Name", "Dummy Email");
	}

}
