package com.epam.libraryservice.client;

import java.util.List;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.epam.libraryservice.client.fallback.BookServiceFallback;
import com.epam.libraryservice.model.Book;

@FeignClient(name = "book-service" ,fallbackFactory=BookServiceFallback.class)
@Component
public interface BookServiceClient {

	@GetMapping("/books")
	ResponseEntity<List<Book>> getBooks();

	@GetMapping("/books/{bookId}")
	ResponseEntity<Book> get(@PathVariable("bookId") Long id);

	@PostMapping("/books")
	ResponseEntity<Book> addNew(@RequestBody Book book);

	@PutMapping("/books/{bookId}")
	ResponseEntity<Book> update(@PathVariable Long bookId, @RequestBody Book book);

	@DeleteMapping("{bookId}")
	ResponseEntity<?> delete(@PathVariable Long bookId);
}
