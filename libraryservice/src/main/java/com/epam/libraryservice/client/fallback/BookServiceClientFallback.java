package com.epam.libraryservice.client.fallback;

import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.ResponseEntity;
import com.epam.libraryservice.client.BookServiceClient;
import com.epam.libraryservice.model.Book;
import feign.FeignException;

public class BookServiceClientFallback implements BookServiceClient {

	private final Throwable cause;
	private static final Logger log = LogManager.getLogger(BookServiceClientFallback.class);

	public BookServiceClientFallback(Throwable cause) {
		this.cause = cause;
	}

	@Override
	public ResponseEntity<List<Book>> getBooks() {
		getCauseOfFallback();
		List<Book> bookList = new ArrayList<>();
		bookList.add(getDummyBook());
		return ResponseEntity.ok((bookList));
	}

	@Override
	public ResponseEntity<Book> get(Long id) {
		getCauseOfFallback();
		return ResponseEntity.ok(getDummyBook());
	}

	@Override
	public ResponseEntity<Book> addNew(Book book) {
		getCauseOfFallback();
		return ResponseEntity.ok(getDummyBook());
	}

	@Override
	public ResponseEntity<Book> update(Long bookId, Book book) {
		getCauseOfFallback();
		return ResponseEntity.ok(getDummyBook());
	}

	@Override
	public ResponseEntity<?> delete(Long bookId) {
		getCauseOfFallback();
		return ResponseEntity.ok("Service is Currently Unavailable.Try After Sometime");
	}

	private void getCauseOfFallback() {
		if (cause instanceof FeignException && ((FeignException) cause).status() == 404) {
			log.error("Service Unavilable while fetching all books , Error Message :" + cause.getLocalizedMessage());
		} else {
			log.error("Other Error took place:" + cause.getLocalizedMessage());
		}
	}
	
	private Book getDummyBook()
	{
		return (new Book(11111L, "Dummy Title", "Dummy Genre", "Dummy Author"));
	}

}
