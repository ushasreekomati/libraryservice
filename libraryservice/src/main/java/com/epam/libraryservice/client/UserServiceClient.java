package com.epam.libraryservice.client;

import java.util.List;
import javax.validation.Valid;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import com.epam.libraryservice.model.User;

@FeignClient(name = "user-service")
@Component
public interface UserServiceClient {

	@GetMapping("/users")
	public ResponseEntity<List<User>> getUsers();

	@PostMapping("/users")
	public ResponseEntity<User> addNew(@RequestBody User user);

	@PutMapping("/users/{userId}")
	public ResponseEntity<User> update(@PathVariable Long userId, @RequestBody @Valid User user);

	@GetMapping("/users/{userId}")
	public ResponseEntity<User> get(@PathVariable Long userId);

	@DeleteMapping("/users/{userId}")
	public ResponseEntity<?> delete(@PathVariable Long userId);
}
