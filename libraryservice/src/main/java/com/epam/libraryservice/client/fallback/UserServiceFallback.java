package com.epam.libraryservice.client.fallback;

import org.springframework.stereotype.Component;
import com.epam.libraryservice.client.UserServiceClient;
import feign.hystrix.FallbackFactory;

@Component
public class UserServiceFallback implements FallbackFactory<UserServiceClient> {

	@Override
	public UserServiceClient create(Throwable cause) {
		return new UserServiceClientFallback(cause);
	}

}
