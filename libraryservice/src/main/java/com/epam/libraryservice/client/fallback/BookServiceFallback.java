package com.epam.libraryservice.client.fallback;

import org.springframework.stereotype.Component;
import com.epam.libraryservice.client.BookServiceClient;
import feign.hystrix.FallbackFactory;

@Component
public class BookServiceFallback implements FallbackFactory<BookServiceClient> {

	@Override
	public BookServiceClient create(Throwable cause) {
		return new BookServiceClientFallback(cause);
	}

}
