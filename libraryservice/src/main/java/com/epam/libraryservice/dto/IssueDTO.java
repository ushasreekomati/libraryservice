package com.epam.libraryservice.dto;

import com.epam.libraryservice.entity.Issue;

public class IssueDTO {
	Long issueId;
	Long userId;
	Long bookId;

	public IssueDTO() {
	}

	public IssueDTO(Long issueId, Long userId, Long bookId) {
		this.issueId = issueId;
		this.userId = userId;
		this.bookId = bookId;
	}

	public IssueDTO(Long userId, Long bookId) {
		this.userId = userId;
		this.bookId = bookId;
	}

	public IssueDTO(Issue issueDetails) {
		this.issueId = issueDetails.getIssueId();
		this.userId = issueDetails.getUserId();
		this.bookId = issueDetails.getBookId();
	}

	public Long getIssueId() {
		return issueId;
	}

	public void setIssueId(Long issueId) {
		this.issueId = issueId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getBookId() {
		return bookId;
	}

	public void setBookId(Long bookId) {
		this.bookId = bookId;
	}

	@Override
	public String toString() {
		return "IssueDTO [issueId=" + issueId + ", userId=" + userId + ", bookId=" + bookId + "]";
	}

}
