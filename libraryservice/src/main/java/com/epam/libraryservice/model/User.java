package com.epam.libraryservice.model;

public class User {

	Long userId;
	String name;
	String emailId;

	public User() {
	}

	public User(Long userId, String name, String emailId) {
		this.userId = userId;
		this.name = name;
		this.emailId = emailId;
	}

	public User(String name, String emailId) {
		this.name = name;
		this.emailId = emailId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	@Override
	public String toString() {
		return "User [userId=" + userId + ", name=" + name + ", emailId=" + emailId + "]";
	}

}
