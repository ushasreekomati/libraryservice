package com.epam.libraryservice.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Issue {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	Long issueId;
	Long userId;
	Long bookId;

	public Issue() {
	}

	public Issue(Long issueId, Long userId, Long bookId) {
		this.issueId = issueId;
		this.userId = userId;
		this.bookId = bookId;
	}

	public Issue(Long userId, Long bookId) {
		this.userId = userId;
		this.bookId = bookId;
	}

	public Long getIssueId() {
		return issueId;
	}

	public void setIssueId(Long issueId) {
		this.issueId = issueId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getBookId() {
		return bookId;
	}

	public void setBookId(Long bookId) {
		this.bookId = bookId;
	}

	@Override
	public String toString() {
		return "Issue [issueId=" + issueId + ", userId=" + userId + ", bookId=" + bookId + "]";
	}

}
