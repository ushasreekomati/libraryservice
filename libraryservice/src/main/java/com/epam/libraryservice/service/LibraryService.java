package com.epam.libraryservice.service;

import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import com.epam.libraryservice.client.BookServiceClient;
import com.epam.libraryservice.client.UserServiceClient;
import com.epam.libraryservice.dto.IssueDTO;
import com.epam.libraryservice.entity.Issue;
import com.epam.libraryservice.exception.BookAlreadyIssuedException;
import com.epam.libraryservice.exception.InvalidIssueDetailsException;
import com.epam.libraryservice.model.Book;
import com.epam.libraryservice.model.User;
import com.epam.libraryservice.repository.IssueRespository;
import org.springframework.stereotype.Service;

@Service
public class LibraryService {

	@Autowired
	private BookServiceClient bookServiceClient;

	@Autowired
	private UserServiceClient userServiceClient;

	@Autowired
	private IssueRespository issueRepository;

	public ResponseEntity<List<Book>> getBooks() {

		return bookServiceClient.getBooks();
	}

	public ResponseEntity<Book> addNew(@RequestBody Book book) {
		return bookServiceClient.addNew(book);

	}

	public ResponseEntity<Book> update(@PathVariable Long bookId, @RequestBody Book book) {
		return bookServiceClient.update(bookId, book);
	}

	public ResponseEntity<Book> getBook(@PathVariable Long bookId) {
		return bookServiceClient.get(bookId);
	}

	public ResponseEntity<?> deleteBook(@PathVariable Long bookId) {
		return bookServiceClient.delete(bookId);

	}

	public ResponseEntity<List<User>> getUsers() {
		return userServiceClient.getUsers();
	}

	public ResponseEntity<User> addNewUser(@RequestBody User user) {
		return userServiceClient.addNew(user);

	}

	public ResponseEntity<User> update(@PathVariable Long userId, @RequestBody @Valid User user) {
		return userServiceClient.update(userId, user);
	}

	public ResponseEntity<User> getUser(@PathVariable Long userId) {
		return userServiceClient.get(userId);
	}

	public ResponseEntity<?> deleteUser(@PathVariable Long userId) {
		return userServiceClient.delete(userId);

	}

	public IssueDTO issueBookToAUser(IssueDTO issueDTO) {

		validateUserId(issueDTO.getUserId());
		validateBookId(issueDTO.getBookId());
		if (issueRepository.existsByUserIdAndBookId(issueDTO.getUserId(), issueDTO.getBookId())) {

			throw new BookAlreadyIssuedException("Book with bookId: " + issueDTO.getBookId()
					+ " is already issued to user with userID: " + issueDTO.getUserId());
		}
		Issue issueDetails = new Issue(issueDTO.getUserId(), issueDTO.getBookId());
		return new IssueDTO(issueRepository.save(issueDetails));

	}

	public void deleteIssue(Long userId, Long bookId) {
		validateUserId(userId);
		validateBookId(bookId);
		if ((!issueRepository.existsByUserIdAndBookId(userId, bookId))) {

			throw new InvalidIssueDetailsException("Invalid Issue Details. Book with bookId: " + bookId
					+ " did not issue to user with userID: " + userId);
		}
		issueRepository.deleteByUserIdAndBookId(userId, bookId);

	}

	public boolean validateUserId(Long userId) {
		return userId.equals(userServiceClient.get(userId).getBody().getUserId());
	}

	public boolean validateBookId(Long bookId) {
		return bookId.equals(bookServiceClient.get(bookId).getBody().getId());
	}
	public void deleteIssuesOfUser(Long userId) {
		issueRepository.deleteByUserId(userId);
	}

	public void deleteIssuesOfBook(Long bookId) {
		issueRepository.deleteByBookId(bookId);
	}


}
