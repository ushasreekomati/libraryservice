package com.epam.libraryservice.controller;

import java.net.URI;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import com.epam.libraryservice.dto.IssueDTO;
import com.epam.libraryservice.model.Book;
import com.epam.libraryservice.model.User;
import com.epam.libraryservice.service.LibraryService;

@RequestMapping("/library")
@RestController
public class LibraryController {

	@Autowired
	LibraryService libraryService;

	@GetMapping("/books")
	public ResponseEntity<List<Book>> getBooks() {
		return libraryService.getBooks();
	}

	@PostMapping("/books")
	public ResponseEntity<Book> addNew(@RequestBody Book book) {
		return libraryService.addNew(book);
	}

	@PutMapping("/books/{bookId}")
	public ResponseEntity<Book> update(@PathVariable Long bookId, @RequestBody Book book) {
		return libraryService.update(bookId, book);
	}

	@GetMapping("/books/{bookId}")
	public ResponseEntity<Book> getBook(@PathVariable Long bookId) {
		return libraryService.getBook(bookId);
	}

	@DeleteMapping("/books/{bookId}")
	public ResponseEntity<?> deleteBook(@PathVariable Long bookId) {
		libraryService.deleteIssuesOfBook(bookId);
		return libraryService.deleteBook(bookId);
	}

	@GetMapping("/users")
	public ResponseEntity<List<User>> getUsers() {
		return libraryService.getUsers();
	}

	@PostMapping("/users")
	public ResponseEntity<User> addNewUser(@RequestBody User user) {
		return libraryService.addNewUser(user);
	}

	@PutMapping("/users/{userId}")
	public ResponseEntity<User> update(@PathVariable Long userId, @RequestBody @Valid User user) {

		return libraryService.update(userId, user);

	}

	@GetMapping("/users/{userId}")
	public ResponseEntity<User> getUser(@PathVariable Long userId) {
		return libraryService.getUser(userId);
	}

	@DeleteMapping("/users/{userId}")
	public ResponseEntity<?> delete(@PathVariable Long userId) {

		libraryService.deleteIssuesOfUser(userId);
		return libraryService.deleteUser(userId);
	}

	@PostMapping("/users/{userId}/books/{bookId}")
	public ResponseEntity<IssueDTO> issueBookForUser(@PathVariable Long userId, @PathVariable Long bookId) {
		IssueDTO issueDTO = new IssueDTO(userId, bookId);

		IssueDTO savedIssueDetails = libraryService.issueBookToAUser(issueDTO);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/id")
				.buildAndExpand(savedIssueDetails.getIssueId()).toUri();
		return ResponseEntity.created(location).body(savedIssueDetails);

	}

	@DeleteMapping("/users/{userId}/books/{bookId}")
	public ResponseEntity<?> deleteIssue(@PathVariable Long userId, @PathVariable Long bookId) {
		libraryService.deleteIssue(userId, bookId);
		return ResponseEntity.noContent().build();
	}

}
