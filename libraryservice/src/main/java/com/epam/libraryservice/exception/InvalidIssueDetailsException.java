package com.epam.libraryservice.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class InvalidIssueDetailsException extends RuntimeException {
	private static final long serialVersionUID = -756115004517137590L;

	public InvalidIssueDetailsException(String message) {
		super(message);
	}
}