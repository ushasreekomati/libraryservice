package com.epam.libraryservice.repository;

import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import com.epam.libraryservice.entity.Issue;

public interface IssueRespository extends JpaRepository<Issue, Long> {

	boolean existsByUserIdAndBookId(Long userId, Long bookId);

	@Transactional
	void deleteByUserIdAndBookId(Long userId, Long bookId);
	
	@Transactional
	void deleteByUserId(Long userId);

	@Transactional
	void deleteByBookId(Long bookId);

}
