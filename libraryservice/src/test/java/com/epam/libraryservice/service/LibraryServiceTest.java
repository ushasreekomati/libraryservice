package com.epam.libraryservice.service;

import static org.junit.jupiter.api.Assertions.*;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willDoNothing;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import com.epam.libraryservice.client.BookServiceClient;
import com.epam.libraryservice.client.UserServiceClient;
import com.epam.libraryservice.model.Book;
import com.epam.libraryservice.model.User;
import com.epam.libraryservice.repository.IssueRespository;

@ExtendWith(SpringExtension.class)
@SpringBootTest(properties = { "feign.hystrix.enabled=true" })
class LibraryServiceTest {

	@Mock
	private BookServiceClient mockBookServiceClient;

	@Mock
	private UserServiceClient mockUserServiceClient;

	@Mock
	private IssueRespository issueRepository;

	@InjectMocks
	private LibraryService libraryService;

	@Test
	public void testGetBooks() throws Exception {
		List<Book> bookList = new ArrayList<>();
		bookList.add(new Book(1L, "Name1", "Genre", "Author"));
		bookList.add(new Book(2L, "Name2", "Genre2", "Author"));
		given(mockBookServiceClient.getBooks()).willReturn(ResponseEntity.ok(bookList));
		assertEquals(libraryService.getBooks(), ResponseEntity.ok(bookList));

	}

	@Test
	public void testGetBookByBookId() throws Exception {

		Book book = new Book(1L, "Name1", "Genre", "Author");
		given(mockBookServiceClient.get(1L)).willReturn(ResponseEntity.ok(book));
		assertEquals(libraryService.getBook(1L), ResponseEntity.ok(book));

	}

	@Test
	public void testUpdateBook() throws Exception {

		Book book = new Book(1L, "Name1", "Genre", "Author");
		given(mockBookServiceClient.update(1L, book)).willReturn(ResponseEntity.ok(book));
		assertEquals(libraryService.update(1L, book), ResponseEntity.ok(book));

	}

	@Test
	public void testDelete() throws Exception {
		given(mockBookServiceClient.delete(1L)).willReturn(ResponseEntity.noContent().build());
		assertEquals(libraryService.deleteBook(1L), ResponseEntity.noContent().build());

	}

	@Test
	public void testAddBook() throws Exception {

		Book book = new Book(1L, "Name1", "Genre", "Author");
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/id").buildAndExpand(book.getId())
				.toUri();
		given(mockBookServiceClient.addNew(book)).willReturn(ResponseEntity.created(location).body(book));
		assertEquals(libraryService.addNew(book), ResponseEntity.created(location).body(book));

	}

	@Test
	public void testGetUsers() throws Exception {
		List<User> userList = new ArrayList<>();
		userList.add(new User(1L, "Name1", "Email"));
		userList.add(new User(2L, "Name2", "Email2"));
		given(mockUserServiceClient.getUsers()).willReturn(ResponseEntity.ok(userList));
		assertEquals(libraryService.getUsers(), ResponseEntity.ok(userList));

	}

	@Test
	public void testGetUserByUserId() throws Exception {

		User user = new User(1L, "Name1", "Email");
		given(mockUserServiceClient.get(1L)).willReturn(ResponseEntity.ok(user));
		assertEquals(libraryService.getUser(1L), ResponseEntity.ok(user));

	}

	@Test
	public void testUpdateUser() throws Exception {

		User user = new User(1L, "Name1", "Email");
		given(mockUserServiceClient.update(1L, user)).willReturn(ResponseEntity.ok(user));
		assertEquals(libraryService.update(1L, user), ResponseEntity.ok(user));

	}

	@Test
	public void testUserDelete() throws Exception {
		given(mockUserServiceClient.delete(1L)).willReturn(ResponseEntity.noContent().build());
		assertEquals(libraryService.deleteUser(1L), ResponseEntity.noContent().build());

	}

	@Test
	public void testAddUser() throws Exception {

		User user = new User(1L, "Name1", "Email");
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/id").buildAndExpand(user.getUserId())
				.toUri();
		given(mockUserServiceClient.addNew(user)).willReturn(ResponseEntity.created(location).body(user));
		assertEquals(libraryService.addNewUser(user), ResponseEntity.created(location).body(user));

	}

	@Test
	void testDeleteIssuesOfUser() throws Exception {
		willDoNothing().given(this.issueRepository).deleteByUserId(any(Long.class));
		libraryService.deleteIssuesOfUser(1L);
	}

	@Test
	void testDeleteIssuesOfBook() throws Exception {
		willDoNothing().given(this.issueRepository).deleteByBookId(any(Long.class));
		libraryService.deleteIssuesOfBook(1L);
	}

}
