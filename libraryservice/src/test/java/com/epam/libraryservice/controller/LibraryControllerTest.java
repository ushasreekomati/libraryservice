package com.epam.libraryservice.controller;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import com.epam.libraryservice.dto.IssueDTO;
import com.epam.libraryservice.model.Book;
import com.epam.libraryservice.model.User;
import com.epam.libraryservice.service.LibraryService;
import com.fasterxml.jackson.databind.ObjectMapper;

@WebMvcTest(LibraryController.class)
class LibraryControllerTest {

	@Autowired
	private MockMvc mvc;

	@MockBean
	private LibraryService libraryService;

	@Test
	void givenUsers_whenGetUsers_thenStatus200() throws Exception {
		List<User> users = new ArrayList<>();
		users.add(new User(1L, "Name1", "EmailId"));
		users.add(new User(2L, "Name2", "EmailId2"));
		given(this.libraryService.getUsers()).willReturn(ResponseEntity.ok(users));
		this.mvc.perform(get("/library/users").accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON));
	}

	@Test
	void givenUser_whenGetUserById_thenStatus200() throws Exception {
		User user = new User(1L, "Name", "EmailId");

		given(this.libraryService.getUser(1L)).willReturn(ResponseEntity.ok(user));

		this.mvc.perform(get("/library/users/1").accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON)).andExpect(jsonPath("$.name", is("Name")));
	}

	@Test
	void givenUser_whenUpdateUser_thenStatus200() throws Exception {
		User user = new User(1L, "Updated Name", "Updated EmailId");
		given(this.libraryService.update(anyLong(), any(User.class))).willReturn(ResponseEntity.ok(user));

		String userJson = "{\"userId\":1, \"name\": \"Updated Name\", \"emailId\": \"Updated EmailId\"}";
		final MockHttpServletRequestBuilder request = put("/library/users/1").content(userJson)
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON);

		this.mvc.perform(request).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.name", is("Updated Name")))
				.andExpect(jsonPath("$.emailId", is("Updated EmailId")));

	}

	@Test
	void givenUser_whenDeleteUser_thenStatus204() throws Exception {
		given(this.libraryService.deleteUser(1L)).willReturn(ResponseEntity.noContent().build());
		this.mvc.perform(delete("/library/users/1")).andExpect(status().isNoContent());
	}

	@Test
	void givenBooks_whenGetBooks_thenStatus200() throws Exception {
		List<Book> books = new ArrayList<>();
		books.add(new Book(1L, "Name1", "Genre", "author"));
		books.add(new Book(2L, "Name2", "Genre", "author"));
		given(this.libraryService.getBooks()).willReturn(ResponseEntity.ok(books));
		this.mvc.perform(get("/library/books").accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON));
	}

	@Test
	void givenBook_whenGetBookById_thenStatus200() throws Exception {
		Book book = new Book(1L, "Name", "Genre", "Author");
		given(this.libraryService.getBook(1L)).willReturn(ResponseEntity.ok(book));
		this.mvc.perform(get("/library/books/1").accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.title", is("Name")));
	}

	@Test
	void givenBook_whenUpdateBook_thenStatus200() throws Exception {
		Book book = new Book(1L, "Updated Name", "Updated Genre", "Updated Author");
		given(this.libraryService.update(anyLong(), any(Book.class))).willReturn(ResponseEntity.ok(book));
		when(libraryService.update(1L, book)).thenReturn(ResponseEntity.ok(book));
		this.mvc.perform(put("/library/books/1").contentType("application/json").content(asJsonString(book)))
				.andExpect(status().isOk());
	}

	@Test
	public void testAddBook() throws Exception {

		Book book1 = new Book();
		book1.setGenre("Genre 1");
		book1.setAuthor("Author 1");
		when(libraryService.addNew(any(Book.class))).thenReturn(new ResponseEntity<Book>(book1, HttpStatus.CREATED));
		mvc.perform(post("/library/books").contentType("application/json").content(asJsonString(book1)))
				.andExpect(status().isCreated());
	}

	@Test
	public void testAddUser() throws Exception {

		User user1 = new User("Name", "Email");
		when(libraryService.addNewUser(any(User.class)))
				.thenReturn(new ResponseEntity<User>(user1, HttpStatus.CREATED));
		mvc.perform(post("/library/users").contentType("application/json").content(asJsonString(user1)))
				.andExpect(status().isCreated());
	}

	@Test
	void givenBook_whenDeleteBook_thenStatus204() throws Exception {
		given(this.libraryService.deleteBook(1L)).willReturn(ResponseEntity.noContent().build());
		this.mvc.perform(delete("/library/books/1")).andExpect(status().isNoContent());
	}

	@Test
	void givenIssue_whenDeleteIssue_thenStatus204() throws Exception {
		Mockito.doNothing().when(this.libraryService).deleteIssue(any(Long.class), any(Long.class));
		this.mvc.perform(delete("/library/users/1/books/1")).andExpect(status().isNoContent());
	}

	@Test
	public void testAddIssue() throws Exception {

		IssueDTO issue = new IssueDTO();
		issue.setIssueId(1L);
		issue.setBookId(1L);
		issue.setBookId(1L);
		when(libraryService.issueBookToAUser(issue)).thenReturn(issue);
		mvc.perform(post("/library/users/1/books/1").contentType("application/json").content(asJsonString(issue)))
				.andExpect(status().isCreated());
	}

	public static String asJsonString(final Object obj) {
		try {
			return new ObjectMapper().writeValueAsString(obj);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
